﻿using System;

namespace ApsiyonLogger.Core.Data
{
    public class Log
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public string LogData { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
