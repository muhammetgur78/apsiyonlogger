﻿using Microsoft.EntityFrameworkCore;

namespace ApsiyonLogger.Core.Data
{
    public class ApsiyonLoggerDbContext : DbContext
    {
        public ApsiyonLoggerDbContext(DbContextOptions<ApsiyonLoggerDbContext> options) : base(options)
        {
        }
        public virtual DbSet<Log> Logs { get; set; }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Log>()
                .Property(b => b.CreateDate)
                .HasDefaultValueSql("getdate()");

        }
    }
}