﻿using Autofac;
using ApsiyonLogger.Core.Repository;
using ApsiyonLogger.Core.Service.Web;
using ApsiyonLogger.Core.Service.Web.Interfaces;

namespace ApsiyonLogger.Core.Ioc
{
    public class AutofacModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Logger>().As<ILogger>().InstancePerDependency();
            builder.RegisterType<ApsiyonLoggerUnitOfWork>().As<IApsiyonLoggerUnitOfWork>().InstancePerDependency();

            base.Load(builder);
        }
    }
}
