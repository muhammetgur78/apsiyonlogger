﻿namespace ApsiyonLogger.Dto.Api
{
    public class LogDto
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public string LogData { get; set; }
    }
}
