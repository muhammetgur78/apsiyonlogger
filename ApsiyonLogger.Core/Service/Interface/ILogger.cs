﻿namespace ApsiyonLogger.Core.Service.Web.Interfaces
{
    public interface ILogger
    {
        void Add(string message, object obj);
    }
}
