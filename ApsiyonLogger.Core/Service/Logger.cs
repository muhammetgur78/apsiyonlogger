﻿using Newtonsoft.Json;
using ApsiyonLogger.Core.Repository;
using System.IO;
using ApsiyonLogger.Core.Data;
using ApsiyonLogger.Core.Service.Web.Interfaces;
using System.Threading.Tasks;

namespace ApsiyonLogger.Core.Service.Web
{
    public class Logger : ILogger
    {
        private readonly IApsiyonLoggerUnitOfWork _apsiyonLoggerUnitOfWork;

        public Logger(IApsiyonLoggerUnitOfWork ApsiyonLoggerUnitOfWork)
        {
            _apsiyonLoggerUnitOfWork = ApsiyonLoggerUnitOfWork;
        }

        public async void Add(string message, object obj)
        {
            try
            {
                _apsiyonLoggerUnitOfWork.Log.Insert(new Log { Message = message, LogData = JsonConvert.SerializeObject(obj) });
                _apsiyonLoggerUnitOfWork.Save();
            }
            catch
            {
                await WriteToFile(message, obj);
            }
        }

        private async Task WriteToFile(string message, object obj)
        {
            var logPath = $"{Directory.GetCurrentDirectory()}/Logs";
            if (!File.Exists(logPath))
            {
                Directory.CreateDirectory(logPath);
            }

            using (StreamWriter streamWriter = new StreamWriter($"{logPath}/log_{System.DateTime.Now.ToString("ddMMyyyy")}.txt", true))
            {
                await streamWriter.WriteLineAsync($"Message : {message} | Data : {JsonConvert.SerializeObject(obj)}");
            }
        }
    }
}
