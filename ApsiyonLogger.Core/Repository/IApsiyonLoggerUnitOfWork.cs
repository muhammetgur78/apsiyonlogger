﻿using System;
using ApsiyonLogger.Core.Data;

namespace ApsiyonLogger.Core.Repository
{
    public interface IApsiyonLoggerUnitOfWork : IDisposable
    {
        void Save();
        GenericRepository<Log> Log { get; }
    }
}
