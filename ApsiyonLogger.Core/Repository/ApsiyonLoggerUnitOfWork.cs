﻿using System;
using ApsiyonLogger.Core.Data;

namespace ApsiyonLogger.Core.Repository
{
    public class ApsiyonLoggerUnitOfWork : IApsiyonLoggerUnitOfWork
    {
        private GenericRepository<Log> _log;

        public readonly ApsiyonLoggerDbContext DbContext;

        private bool _disposed = false;
        public ApsiyonLoggerUnitOfWork(ApsiyonLoggerDbContext context)
        {
            DbContext = context;
        }
        public GenericRepository<Log> Log => _log ?? (_log = new GenericRepository<Log>(DbContext));

        public async void Save()
        {

            try
            {
               DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    DbContext.Dispose();
                }
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}