﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ApsiyonLogger.Core.Repository
{
    public interface IGenericRepository<T>
        where T : class
    {
        Task<T> FindByIdAsync(object EntityId);
        T FindById(object entityId);
        IEnumerable<T> Where(Expression<Func<T, bool>> filter = null, params Expression<Func<T, object>>[] includes);
        Task<IEnumerable<T>> GetAllAsync(Expression<Func<T, bool>> filter = null);
        IEnumerable<T> GetAll(Expression<Func<T, bool>> filter = null);
        Task<T> SingleOrDefaultAsync(Expression<Func<T, bool>> filter = null);
        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> filter = null);
        Task InsertAsync(T entity);
        void Insert(T entity);
        void Update(T Entity);
        Task DeleteAsync(object EntityId);
        void Delete(T Entity);
        Task<IEnumerable<T>> ToListAsync();
        IQueryable<T> AsQueryable();
        Task<bool> AnyAsync(Expression<Func<T, bool>> filter = null);
        Task<int> CountAsync(Expression<Func<T, bool>> filter = null);
    }
}
