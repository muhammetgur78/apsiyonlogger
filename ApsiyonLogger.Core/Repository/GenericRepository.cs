﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ApsiyonLogger.Core.Data;


namespace ApsiyonLogger.Core.Repository
{
    public class GenericRepository<T> : IGenericRepository<T>
        where T : class
    {
        private readonly ApsiyonLoggerDbContext _context;
        private readonly DbSet<T> _dbSet;

        public GenericRepository(ApsiyonLoggerDbContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public virtual async Task<T> FindByIdAsync(object entityId)
        {
            return await _dbSet.FindAsync(entityId);
        }

        public virtual T FindById(object entityId)
        {
            return _dbSet.Find(entityId);
        }

        public virtual IEnumerable<T> Where(Expression<Func<T, bool>> filter = null, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query;
            if (filter != null)
            {
                query = _dbSet.Where(filter).AsNoTracking();
                return includes.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
            }

            return _dbSet;
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync(Expression<Func<T, bool>> filter = null)
        {
            if (filter != null)
            {
                return await _dbSet.Where(filter).AsNoTracking().ToListAsync();
            }

            return await _dbSet.AsNoTracking().ToListAsync();
        }

        public virtual IEnumerable<T> GetAll(Expression<Func<T, bool>> filter = null)
        {
            if (filter != null)
            {
                return _dbSet.Where(filter).AsNoTracking().ToList();
            }

            return _dbSet.AsNoTracking().ToList();
        }

        public virtual async Task<T> SingleOrDefaultAsync(Expression<Func<T, bool>> filter = null)
        {
            return await _dbSet.Where(filter).AsNoTracking().SingleOrDefaultAsync();
        }

        public virtual async Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> filter = null)
        {
            return await _dbSet.Where(filter).AsNoTracking().FirstOrDefaultAsync();
        }

        public virtual async Task InsertAsync(T entity)
        {
            await _dbSet.AddAsync(entity);
        }

        public virtual void Insert(T entity)
        {
            _dbSet.Add(entity);
        }

        public virtual void Update(T entityToUpdate)
        {
            _dbSet.Attach(entityToUpdate);
            _context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public virtual async Task DeleteAsync(object entityId)
        {
            T entityToDelete = await _dbSet.FindAsync(entityId);
            Delete(entityToDelete);
        }

        public virtual void Delete(T entity)
        {
            if (_context.Entry(entity).State == EntityState.Detached)
            {
                _dbSet.Attach(entity);
            }

            _dbSet.Remove(entity);
        }

        public async Task<IEnumerable<T>> ToListAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public IQueryable<T> AsQueryable()
        {
            return _dbSet.AsQueryable();
        }

        public async Task<bool> AnyAsync(Expression<Func<T, bool>> filter = null)
        {
            return await _dbSet.AnyAsync(filter);
        }

        public async Task<int> CountAsync(Expression<Func<T, bool>> filter = null)
        {
            return await _dbSet.CountAsync(filter);
        }
    }
}
