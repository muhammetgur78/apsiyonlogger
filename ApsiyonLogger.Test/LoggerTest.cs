using ApsiyonLogger.Core.Service.Web;
using System;
using System.IO;
using System.Linq;
using Xunit;

namespace ApsiyonLogger.Test
{
    public class LoggerTest : TestBase
    {
        [Fact]
        public void AddLog_To_Database_Test()
        {
            _logger.Add("Log Message", new { Id = 1, Name = "Muhammet", LastName = "G�r" });

            var logs = _apsiyonLoggerUow.Log.GetAll();

            Assert.True(logs.Count() > 0);
        }

        [Fact]
        public void AddLog_To_File_Test()
        {

            _logger = new Logger(null);
            _logger.Add("Log Message", new { Id = 1, Name = "Muhammet", LastName = "G�r" });

            var logPath = Directory.GetCurrentDirectory() + "/Logs/log_" + DateTime.Now.ToString("ddMMyyyy") + ".txt";
            var logFileExists = File.Exists(logPath);
            File.Delete(logPath);

            Assert.True(logFileExists);
        }
    }
}
