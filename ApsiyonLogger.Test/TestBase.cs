﻿using ApsiyonLogger.Core.Data;
using ApsiyonLogger.Core.Repository;
using ApsiyonLogger.Core.Service.Web;
using ApsiyonLogger.Core.Service.Web.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;

namespace ApsiyonLogger.Test
{
    public abstract class TestBase
    {
        public ApsiyonLoggerDbContext _dbContext;
        public IApsiyonLoggerUnitOfWork _apsiyonLoggerUow;
        public ILogger _logger;

        public TestBase()
        {
            var options = new DbContextOptionsBuilder<ApsiyonLoggerDbContext>()
                                  .UseInMemoryDatabase(Guid.NewGuid().ToString())
                                  .Options;
            _dbContext= new ApsiyonLoggerDbContext(options);
          
            _apsiyonLoggerUow = new ApsiyonLoggerUnitOfWork(_dbContext);

            _logger = new Logger(_apsiyonLoggerUow);
        }
    }
}
