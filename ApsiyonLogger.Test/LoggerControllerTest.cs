using ApsiyonLogger.Api.Controllers;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ApsiyonLogger.Test
{
    public class LoggerControllerTest : TestBase
    {
        [Fact]
        public async Task LoggerController_Add_Test()
        {
            var loggerController = new LoggerController(_logger);
            await loggerController.Add();

            var logs = await _apsiyonLoggerUow.Log.GetAllAsync();

            Assert.True(logs.Count() > 0);
        }
    }
}
