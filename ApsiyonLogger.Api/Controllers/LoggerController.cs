﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ApsiyonLogger.Core.Service.Web.Interfaces;

namespace ApsiyonLogger.Api.Controllers
{
    [ApiController]
    public class LoggerController : ControllerBase
    {
        private readonly ILogger _logger;

        public LoggerController(ILogger logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [Route("Logger/Add")]
        public async Task Add()
        {
            _logger.Add("test", new { errorCode = 1, message = "info test" });
        }
        
    }
}
